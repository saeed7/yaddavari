package ir.eyerun.moradi.yaddavari;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by saeed on 18/04/2018.
 */

    public class YaddavariSimpleCursorAdapter extends SimpleCursorAdapter {
        public YaddavariSimpleCursorAdapter(Context context, int layout, Cursor c, String[]
                from, int[] to, int flags) {
            super(context, layout, c, from, to, flags);
        }
    //baraye estefade az yek binnande, shoma bayad az 2 metode zir estefade konid v kelase  ViewHolder ra tarif konid
        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            return super.newView(context, cursor, parent);
        }
        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            super.bindView(view, context, cursor);
            ViewHolder holder = (ViewHolder) view.getTag();
            if (holder == null) {
                holder = new ViewHolder();
                holder.colImp = cursor.getColumnIndexOrThrow(YaddavariDbAdapter.COL_IMPORTANT);
                holder.listTab = view.findViewById(R.id.row_tab);
                view.setTag(holder);
            }
            if (cursor.getInt(holder.colImp) > 0) {
                holder.listTab.setBackgroundColor(context.getResources().getColor(R.color.orange));
            } else {
                holder.listTab.setBackgroundColor(context.getResources().getColor(R.color.green));
            }
        }
        static class ViewHolder {
            //soton ra zakhire kon
            int colImp;
            //namayesh ra zakhire kon
            View listTab;
        }
    }